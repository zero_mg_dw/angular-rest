import {Component} from '@angular/core';
import {EstudianteService} from "./estudiante.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hola mundo !!!';
  listadoEstudiante: any;

  constructor(private estudianteService: EstudianteService) {

  }

  public metodoPrueba(): void {
    this.estudianteService.callRemoteService().subscribe((result) => {
      console.log('datos de server');
      console.log(result);

      this.listadoEstudiante = result;

    });
  }
}
