import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  constructor(private http: HttpClient) { }

  public hola(): void {
    console.log('hola');
  }

  public callRemoteService(): Observable<any>{
    let url = 'http://localhost:8585/estudiante/all';
    return this.http.get(url);
  }


}
